package com.company;

import com.company.lang.Point;
import com.company.lang.Point2;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.*;
import java.util.*;

public class Main {
	public static final String INPUT_FILENAME = "chips.txt";
	public static final String OUTPUT_FILENAME = "out.txt";
	public static final String OUTPUT_EXCEL_FILENAME = "out.xlsx";
	public static final double C = 4d / 5d;

	public static void main(String[] args) {
		try {
			FileReader in = new FileReader(INPUT_FILENAME);
			Point2[] points2 = readPoints2(in);
			Map<Integer, Integer> result = kNN(points2);
			writeDataInStream(new FileWriter(OUTPUT_FILENAME), result);
			writeDataInExcel(result);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Point2[] readPoints2(InputStreamReader reader) {
		List<Point2> list = new ArrayList<>();
		try (BufferedReader in = new BufferedReader(reader)) {
			in.readLine();
			String s;
			String[] split;
			while ((s = in.readLine()) != null) {
				if (s.isEmpty()) continue;
				s = s.replaceAll(",", ".");
				split = s.split("\\s");
				list.add(
						new Point2(
								Double.parseDouble(split[0]),
								Double.parseDouble(split[1]),
								Integer.parseInt(split[2])
						)
				);
			}
			return list.toArray(new Point2[list.size()]);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return (Point2[]) list.toArray();
	}

	public static Map<Integer, Integer> kNN(Point2[] points2) {
		Map<Integer, Integer> result = new HashMap<>();
		Log.sendMessage("Array of points mixes");
		for (int i = 0; i < 100; i++) {
			points2 = mixingArray(points2);
		}
		Log.sendMessage("Array of points is ready");
		int countLearnPoint = (int) (C * (double) points2.length);
		int countTestPoint = points2.length - countLearnPoint;
		Log.sendMessage("Max K : " + countLearnPoint);

		for (int k = 2; k <= countLearnPoint; k++) {
			List<Integer> percentsRight = new ArrayList<>();
			for (int m = 0; m < 400; m++) {
				points2 = mixingArray(points2);
				int percentRight;
				int countRightClassification = 0;
				for (int i = 0; i < countTestPoint; i++) {
					Point2 testPoint = points2[countLearnPoint + i];
					List<Point2> list = new ArrayList<>();
					Collections.addAll(list, points2);
					Collections.sort(list, (o1, o2) -> {
						double k1 = o1.distance(testPoint) - o2.distance(testPoint);
						if (k1 == 0) return 0;
						else if (k1 > 0) return 1;
						else return -1;
					});
					int sum = 0;
					for (int j = 0; j < k; j++) {
						Point point = list.get(j);
						sum += point.getNumClass();
					}
					int defineClass = (sum >= k / 2) ? 1 : 0;
					if (defineClass == testPoint.getNumClass()) countRightClassification++;
				}
				percentRight = (int) ((double) countRightClassification * 100d / (double) countTestPoint);
				percentsRight.add(percentRight);
			}

			result.put(k, arithAverage(percentsRight));
		}
		Log.sendMessage("DONE");
		return result;
	}

	public static void writeDataInStream(OutputStreamWriter writer, Map<Integer, Integer> data) {
		try (PrintWriter out = new PrintWriter(new BufferedWriter(writer))) {
			out.println("k right(%)");
			List<Integer> keys = new ArrayList<>(data.keySet());
			Collections.sort(keys);
			for (int i : keys)
				out.printf("%s %s\n", i, data.get(i));
		}
	}

	public static void writeDataInExcel(Map<Integer, Integer> data) {
		try (FileOutputStream out = new FileOutputStream(OUTPUT_EXCEL_FILENAME)) {
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Result");
			List<Integer> keys = new ArrayList<>(data.keySet());
			Collections.sort(keys);
			int rownum = 0;
			for (int key : keys) {
				Row row = sheet.createRow(rownum++);
				int value = data.get(key);
				Cell cell = row.createCell(0);
				cell.setCellValue(key);
				cell = row.createCell(1);
				cell.setCellValue(value);
			}
			workbook.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static int arithAverage(List<Integer> list) {
		int a = list.stream().reduce(0, (integer, integer2) -> integer + integer2);
		return a / list.size();
	}

	public static Point2[] mixingArray(Point2[] array) {
		List<Point2> answer = new ArrayList<>();
		List<Point2> list = new ArrayList<>();
		Collections.addAll(list, array);
		while (!list.isEmpty()) {
			int index = new Random().nextInt(list.size());
			answer.add(list.remove(index));
		}
		return answer.toArray(new Point2[answer.size()]);
	}

	static class Log {
		public static void sendMessage(String message) {
			new Thread(() -> System.out.println(message)).start();
		}
	}
}
