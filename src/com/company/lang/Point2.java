package com.company.lang;

public class Point2 extends Point {

	public Point2(double x, double y, int numClass) {
		super(new double[]{x, y}, numClass);
	}

	public double getX() {
		return coordinates[0];
	}

	public double getY() {
		return coordinates[1];
	}

	public double distance(Point2 other) {
		return Math.sqrt(Math.pow(getX() - other.getX(), 2) + Math.pow(getY() - other.getY(), 2));
	}
}
