package com.company.lang;

public class Point {
	protected double[] coordinates;
	protected int numClass;

	public Point(double[] coordinates, int numClass) {
		this.coordinates = coordinates;
		this.numClass = numClass;
	}

	public double[] getCoordinates() {
		return coordinates;
	}

	public double getCoordinatesByIndex(int index) {
		if (index < 0 || index >= coordinates.length) throw new IllegalArgumentException();
		return coordinates[index];
	}

	public int getNumClass() {
		return numClass;
	}

}
